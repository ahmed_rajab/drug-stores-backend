<?php
include('../../connection.php');

$drug_store_id = input('drug_store_id');
$drugs = input('drugs');
$create=true;
if(input('order_id')!=null){
    $order_id=input('order_id');
    $create=false;
}
$rows['result'] = '0';
$suffecient = true;
for ($c = 0; $c < count($drugs); $c++) {
    $drug = $drugs[$c];
    $drugId = $drug->{'id'};
    $drugQuantity = $drug->{'quantity'};
    $query = "select name,quantity from drugs where id=$drugId";
    $result = mysqli_query($con, $query);
    while ($row = mysqli_fetch_assoc($result)) {
        $drugName = $row['name'];
        if ($create) {
            if ($row['quantity'] < $drugQuantity) $suffecient = false;
        } else {
            $query="select * from order_drug where drug_id=$drugId and order_id=$order_id";
            $resultt=mysqli_query($con,$query);
            while ($roww=mysqli_fetch_assoc($resultt))$orderDrugQuantity = $roww['quantity'];
            if ($row['quantity']<$drugQuantity-$orderDrugQuantity)$suffecient = false;
        }
        if($create)$max_quantity = $row['quantity'];
        else $max_quantity=$row['quantity']+$orderDrugQuantity-$drugQuantity;
    }
    if (!$suffecient) {
        $rows['result'] = '1';
        $rows['message'] = messages(['drugName' => $drugName, 'drugQuantity' => $max_quantity])['insuffecient drug quantity'][$appLanguage];
        break;
    }
}

if ($rows['result'] != '1') {
    mysqli_begin_transaction($con);
    try {
        if ($create) {
            $query = "insert into orders (user_id,drug_store_id) values ($userId,$drug_store_id)";
            $result = mysqli_query($con, $query);
            $order_id = mysqli_insert_id($con);
        } else {
            $query = "update orders set drug_store_id = $drug_store_id where id= $order_id";
            $result = mysqli_query($con, $query);
        }
        $query = "update drugs d set quantity = quantity + 
            COALESCE((select quantity from order_drug where order_id=$order_id and drug_id=d.id),0)";
        $result = mysqli_query($con, $query);

        $query = "delete from order_drug where order_id=$order_id";
        $result = mysqli_query($con, $query);
        for ($c = 0; $c < count($drugs); $c++) {
            $drug = $drugs[$c];
            $drugId = $drug->{'id'};
            $drugQuantity = $drug->{'quantity'};

            $query = "insert into order_drug (drug_id,order_id,quantity) values ($drugId,$order_id,$drugQuantity)";
            $result = mysqli_query($con, $query);
            $query = "update drugs set quantity=quantity-$drugQuantity where id = $drugId";
            $result = mysqli_query($con, $query);
        }
    } catch (mysqli_sql_exception $exception) {
        mysqli_rollback($con);
        throw $exception;
    }
}

include('../../output.php');
