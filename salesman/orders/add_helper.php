<?php
include('../../connection.php');

if (input('order_id')!=null){
    $orderId=input('order_id');
    $query="select od.quantity as drugs_count,d.* from order_drug od
    left outer join drugs d on d.id=od.drug_id
    where od.order_id=$orderId";
    $result=mysqli_query($con,$query);
    $orderDrugs=array();
    while ($row=mysqli_fetch_assoc($result)) array_push($orderDrugs, $row);
    $rows['order_drugs']= $orderDrugs;
}

$query = "select id,name,quantity from drugs where app_id=$appId order by created_at desc";
$result = mysqli_query($con, $query);
$drugs = array();
while ($row = mysqli_fetch_assoc($result)) array_push($drugs, $row);

$query = "select id,name from drug_stores where app_id=$appId order by created_at desc";
$result = mysqli_query($con, $query);
$drugStores = array();
while ($row = mysqli_fetch_assoc($result)) array_push($drugStores, $row);

$rows['result'] = '0';
$rows['drugs'] = $drugs;
$rows['drug_stores'] = $drugStores;

include('../../output.php');
