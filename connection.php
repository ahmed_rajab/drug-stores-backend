<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
	// should do a check here to match $_SERVER['HTTP_ORIGIN'] to a
	// whitelist of safe domains
	//header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header("Access-Control-Allow-Origin: *");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
}
ini_set('display_errors', 1);
ini_set('log_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

/*** THIS! ***/
//mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

define('HOST', 'fdb34.awardspace.net');
define('USER', '3984514_drugs');
define('PASSWORD', 'AKU5eoy91');
define('DB', '3984514_drugs');

$con = mysqli_connect(HOST, USER, PASSWORD, DB);

if (!$con) die('no connection: ' . mysqli_connect_error($con));

$selected_database = mysqli_select_db($con, DB) or die('can\'t select database: ' . mysqli_connect_error($con));

mysqli_query($con, 'SET CHARACTER SET utf8mb4');

$deviceInfo = $_SERVER['HTTP_DEVICE_INFO'];
$userId = $_SERVER['HTTP_USER_ID'];
$ipAddress = $_SERVER['REMOTE_ADDR'];

$deviceInfo = json_decode($deviceInfo);
$buildModel = $deviceInfo->{'buildModel'};
$androidAPI = $deviceInfo->{'androidAPI'};
$rooted = $deviceInfo->{'rooted'};
$isEmulator = $deviceInfo->{'isEmulator'};
$appLanguage = $deviceInfo->{'appLanguage'};
$appVersionName = $deviceInfo->{'appVersionName'};
$appVersionCode = $deviceInfo->{'appVersionCode'};
$appId = $deviceInfo->{'appId'};
/*$query="SET @update_id := 0;
	UPDATE devices SET ipAddress = '$ipAddress', id = (SELECT @update_id := id) ,appLanguage='$appLanguage',
	appVersionCode=$appVersionCode,appVersionName='$appVersionName', lastSeen= current_timestamp,
	buildModel='$buildModel',ime1='$ime1',ime2='$ime2',androidAPI='$androidAPI',rooted=$rooted,isEmulator=$isEmulator,appId=$appId 
	WHERE ime1 = '$ime1' and appId=$appId; SELECT @update_id;";
	$result=mysqli_multi_query($con,$query);
if ($result) {
    do {
        // grab the result of the next query
        if (($result = mysqli_store_result($con)) === false && mysqli_error($con) != '') {
            echo "Query failed: " . mysqli_error($con);
        }
    } while (mysqli_more_results($con) && mysqli_next_result($con)); // while there are more results
} else {
    echo "First query failed..." . mysqli_error($con);
}
$deviceId=0;
while($row=mysqli_fetch_assoc($result))$deviceId=$row['@update_id'];

if($deviceId==0){
	$query="insert into devices 
	(buildModel,ime1,ime2,ipAddress,androidAPI,appLanguage,appVersionName,appVersionCode,rooted,isEmulator,appId)values 
	('$buildModel','$ime1','$ime2','$ipAddress','$androidAPI','$appLanguage','$appVersionName',$appVersionCode,$rooted,$isEmulator,$appId)
	";
	$result=mysqli_query($con,$query);
	$deviceId=mysqli_insert_id($con);
}
$rows=array();
$rows['userId']=$deviceId;*/


function messages($params = [])
{
	$drugName='';
	if (isset($params['drugName']))$drugName = $params['drugName'];

	$drugQuantity='';
	if (isset($params['drugQuantity']))$drugQuantity = $params['drugQuantity'];
	return [
		'wrong username or password' => [
			'ar' => 'خطأ في اسم المستخدم أو كلمة السر, تحقق من البيانات المدخلة وحاول مجدداً',
			'en' => 'Wrong input data, check the input data and try again',
		],
		'account already exists' => [
			'ar' => 'هذا الحساب موجود مسبقاً, قم بتسجيل الدخول',
			'en' => 'This account already exists, log in instead',
		],
		'insuffecient drug quantity' => [
			'ar' => "الدواء '$drugName' متوفر بكمية $drugQuantity كحد أقصى",
			'en' => "The drug '$drugName' is available in $drugQuantity maximum"
		]
	];
}

function input($parameter)
{
	if (!isset(json_decode(file_get_contents('php://input'))->{$parameter}))return null;
		$parameter = json_decode(file_get_contents('php://input'))->{$parameter};
		global $con;
		if (is_string($parameter)) {
			$parameter = strip_tags($parameter);
			$parameter = mysqli_real_escape_string($con, $parameter);
		}
		return $parameter;
}
