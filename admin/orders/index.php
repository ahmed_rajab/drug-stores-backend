<?php
include('../../connection.php');

$limit=input('limit');
$offset=input('offset');
						
$query = "SELECT o.id,o.drug_store_id,ds.name as drug_store_name,
u.id as salesman_id, u.full_name as salesman_name,
(select sum(quantity) from order_drug where order_id=o.id) as drugs_count FROM orders o
left outer join drug_stores ds on ds.id=o.drug_store_id
left outer join users u on u.id=o.user_id
where o.user_id in (select id from users where app_id=$appId) order by o.created_at desc limit $limit offset $offset";
	$result=mysqli_query($con,$query);
	
	$orders=array();
	while($row =mysqli_fetch_assoc($result))array_push($orders,$row);
	
	$rows['result']='0';
	$rows['orders']=$orders;
	
	include('../../output.php');
